module Game exposing (..)

import Dict exposing (Dict)

type alias Point = ( Int, Int )

-- player

type alias PlayerState =
    { attacking : Bool
    , items : List ItemType
    }

initPlayerState =
    { attacking = False
    , items = []
    }

playerText : PlayerState -> String
playerText state =
    if state.attacking then "👸🔪" else "👸"

-- objects

type alias Objects = Dict Point Object

type Object
    = Block BlockType
    | Entity EntityType
    | Item ItemType
    | Text String

type BlockType
    = Wall
    | Door Scene DoorCondition

type EntityType
    = Ghost
    | Zombie
    | Alien
    | Monster
    | Butterfly Int Int
    | Bug Int Int

type ItemType
    = Key
    | Wine
    | Yam
    | Popcorn

type DoorCondition
    = NoCondition
    | HasItem ItemType

objectText : Object -> String
objectText object =
    case object of
        Block Wall -> "🧱"
        Block (Door _ _) -> "🚪"
        Entity (Ghost) -> "👻"
        Entity (Zombie) -> "🧟"
        Entity (Alien) -> "👽"
        Entity (Monster) -> "👺"
        Entity (Butterfly _ _) -> "🦋"
        Entity (Bug _ _) -> "🐛"
        Item Key -> "🔑"
        Item Wine -> "🍷"
        Item Yam -> "🍠"
        Item Popcorn -> "🍿"
        Text string -> string

-- game settings

tick = 1000 / 20
boardSize = 20
cellSize = 30
entityAnimation = 1000

controlsUp     = 87 -- w
controlsLeft   = 65 -- a
controlsDown   = 83 -- s
controlsRight  = 68 -- d
controlsAttack = 32 -- space

-- scenes

type alias Scene = ( Point, Objects )

scene1 = ( ( 10, 16 ), objects1 )
objects1 =
    Dict.fromList
        [ ( ( 10, 4 ), Block (Door scene2 NoCondition) )
        ]

scene2 = ( ( 4, 16 ), objects2 )
objects2 =
    Dict.fromList
        [ ( ( 15, 5 ), Block (Door scene3 NoCondition) )
        , ( ( 15, 4 ), Text "hi baby" )
        ]

scene3 = ( ( 10, 4 ), objects3 )
objects3 =
    Dict.fromList
        [ ( ( 9, 7 ), Block Wall )
        , ( ( 10, 7 ), Block Wall )
        , ( ( 11, 7 ), Block Wall )
        , ( ( 7, 8 ), Block Wall )
        , ( ( 8, 8 ), Block Wall )
        , ( ( 9, 8 ), Block Wall )
        , ( ( 10, 8 ), Block Wall )
        , ( ( 11, 8 ), Block Wall )
        , ( ( 12, 8 ), Block Wall )
        , ( ( 8, 9 ), Block Wall )
        , ( ( 9, 9 ), Block Wall )
        , ( ( 10, 9 ), Block Wall )
        , ( ( 11, 9 ), Block Wall )
        , ( ( 10, 10 ), Block Wall )
        , ( ( 10, 16 ), Block (Door scene4 NoCondition) )
        , ( ( 9, 15 ), Text "you're my rock" )
        ]

scene4 = ( ( 4, 16 ), objects4 )
objects4 =
    let
        sun = Text "☀️"
        sunflower = Text "🌻"
    in
    Dict.fromList
        [ ( ( 14, 4 ), sun )
        , ( ( 15, 4 ), sun )
        , ( ( 16, 4 ), sun )
        , ( ( 13, 5 ), sun )
        , ( ( 14, 5 ), sun )
        , ( ( 15, 5 ), sun )
        , ( ( 16, 5 ), sun )
        , ( ( 17, 5 ), sun )
        , ( ( 13, 6 ), sun )
        , ( ( 14, 6 ), sun )
        , ( ( 15, 6 ), sun )
        , ( ( 16, 6 ), sun )
        , ( ( 17, 6 ), sun )
        , ( ( 13, 7 ), sun )
        , ( ( 14, 7 ), sun )
        , ( ( 15, 7 ), sun )
        , ( ( 16, 7 ), sun )
        , ( ( 17, 7 ), sun )
        , ( ( 14, 8 ), sun )
        , ( ( 15, 8 ), sun )
        , ( ( 16, 8 ), sun )
        , ( ( 12, 16 ), sunflower )
        , ( ( 16, 16 ), Block (Door scene5 NoCondition) )
        , ( ( 12, 12 ), Text "and my sunshine" )
        ]

scene5 = ( ( 4, 16 ), objects5 )
objects5 =
    Dict.fromList
        [ ( ( 16, 16 ), Block (Door scene6 NoCondition) )
        , ( ( 7, 8 ), Text "⬆️" )
        , ( ( 8, 8 ), Text "⬅️" )
        , ( ( 9, 8 ), Text "⬇️" )
        , ( ( 10, 8 ), Text "➡️" )
        , ( ( 5, 5 ), Text "I love moving through life with you" )
        , ( ( 6, 6 ), Text "from scene to scene" )
        ]

scene6 = ( ( 4, 16 ), objects6 )
objects6 =
    Dict.fromList
        [ ( ( 16, 16 ), Block (Door scene7 NoCondition) )
        , ( ( 9, 8 ), Text "🌱" )
        , ( ( 7, 9 ), Text "🍃" )
        , ( ( 7, 8 ), Entity (Butterfly 1 0) )
        , ( ( 8, 9 ), Entity (Bug -1 150) )
        , ( ( 5, 5 ), Text "from one growth to the next" )
        ]

scene7 = ( ( 4, 16 ), objects7 )
objects7 =
    Dict.fromList
        [ ( ( 16, 16 ), Block (Door scene8 NoCondition) )
        , ( ( 15, 15 ), Entity (Ghost) )
        , ( ( 16, 15 ), Entity (Ghost) )
        , ( ( 17, 15 ), Entity (Ghost) )
        , ( ( 15, 16 ), Entity (Monster) )
        , ( ( 17, 16 ), Entity (Zombie) )
        , ( ( 15, 17 ), Entity (Ghost) )
        , ( ( 16, 17 ), Entity (Alien) )
        , ( ( 17, 17 ), Entity (Ghost) )
        , ( ( 14, 15 ), Entity (Ghost) )
        , ( ( 14, 16 ), Entity (Monster) )
        , ( ( 14, 17 ), Entity (Alien) )
        , ( ( 13, 15 ), Entity (Ghost) )
        , ( ( 13, 16 ), Entity (Ghost) )
        , ( ( 13, 17 ), Entity (Zombie) )
        , ( ( 5, 5 ), Text "from one challenge to the next" )
        , ( ( 6, 7 ), Text "(space to attack)" )
        ]

scene8 = ( ( 4, 16 ), objects8 )
objects8 =
    Dict.fromList
        [ ( ( 16, 16 ), Block (Door scene9 (HasItem Key)) )
        , ( ( 5, 6 ), Item Key )
        , ( ( 5, 5 ), Text "from grace" )
        , ( ( 8, 8 ), Text "to grace" )
        , ( ( 18, 12 ), Text "🐔✋" )
        ]

scene9 = ( ( 4, 16 ), objects9 )
objects9 =
    Dict.fromList
        [ ( ( 16, 16 ), Block (Door scene10 (HasItem Key)) )
        , ( ( 11, 7 ), Item Wine )
        , ( ( 12, 7 ), Item Yam )
        , ( ( 13, 7 ), Item Yam )
        , ( ( 14, 7 ), Item Popcorn )
        , ( ( 5, 6 ), Item Key )
        , ( ( 5, 5 ), Text "you hold the key to my heart" )
        ]

scene10 = ( ( 10, 12 ), objects10 )
objects10 =
    Dict.fromList
        [ ( ( 5, 5 ), Text "happy valentine's day zooey👑🌹💌" )
        ]

sceneStart = scene1
