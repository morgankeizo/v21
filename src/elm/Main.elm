module Main exposing (main)

import Browser exposing (Document)
import Browser.Events as BE
import Dict
import Html as H exposing (Html)
import Html.Attributes as HA
import Json.Decode as D exposing (Decoder)
import Svg as S exposing (Svg)
import Svg.Attributes as SA
import Set exposing (Set)
import Time exposing (Posix)

import Game exposing (..)

-- main

main =
    Browser.document
    { init = init
    , update = update
    , view = view
    , subscriptions = subscriptions
    }

-- model

type alias Model =
    { playerPosition : Point
    , playerState : PlayerState
    , objects : Objects
    , keys : Set Int
    }

init : () -> ( Model, Cmd Msg )
init _ =
    let
        model =
            { playerPosition = Tuple.first sceneStart
            , playerState = initPlayerState
            , objects = Tuple.second sceneStart
            , keys = Set.empty
            }
    in
    ( model, Cmd.none )

-- view

view : Model -> Document Msg
view model =
    let
        player = viewPlayer model.playerPosition model.playerState
        objects =
            model.objects
            |> Dict.toList
            |> List.map viewObject

        board =
            S.svg
                [ SA.class "board" ]
                (player :: objects)
    in
    { title = "v21"
    , body = [ board ]
    }

viewObject : ( Point, Object ) -> Svg Msg
viewObject ( position, object ) =
    let
        extraAttrs =
            case object of
                Text _ ->
                    [ SA.class "text" ]

                _ ->
                    []
    in
    viewText position extraAttrs (objectText object)

viewPlayer : Point -> PlayerState -> Svg Msg
viewPlayer position state =
    viewText position [] (playerText state)

viewText : Point -> List (S.Attribute Msg) -> String -> Svg Msg
viewText position extraAttrs text =
    S.text_
        ( SA.class "object" :: positionAttrs position ++ extraAttrs )
        [ S.text text ]

positionAttrs : Point -> List (S.Attribute Msg)
positionAttrs ( x, y ) =
    [ SA.x (String.fromInt (x * cellSize))
    , SA.y (String.fromInt (y * cellSize))
    ]

-- subscriptions

subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch
        [ BE.onKeyDown (keyDecoder KeyDown)
        , BE.onKeyUp (keyDecoder KeyUp)
        , Time.every tick Tick
        ]

keyDecoder : (Int -> Msg) -> Decoder Msg
keyDecoder fn =
    D.field "keyCode" D.int
    |> D.map fn

-- update

type Msg
    = KeyDown Int
    | KeyUp Int
    | Tick Posix

type ObjectUpdate
    = RemoveObject Point
    | AddObject Point Object

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        KeyDown key ->
            updateKeys model (Set.insert key)

        KeyUp key ->
            updateKeys model (Set.remove key)

        Tick now ->
            let
                -- move player
                { newPlayerPosition, newPlayerState, objectUpdates1, maybeChangeScene } =
                    updatePlayer model

                -- move entities
                updateObject ( key, object ) =
                    case object of
                        Entity entity ->
                            updateEntity model now key entity

                        _ ->
                            []

                objectUpdates2 =
                    model.objects
                    |> Dict.toList
                    |> List.map updateObject
                    |> List.foldl (++) []

                -- consume object updates
                objectUpdates = objectUpdates1 ++ objectUpdates2
                consumeObjectUpdate objectUpdate runningModel =
                    case objectUpdate of
                        RemoveObject key ->
                            { runningModel
                            | objects = Dict.remove key runningModel.objects
                            }

                        AddObject key object ->
                            { runningModel
                            | objects = Dict.insert key object runningModel.objects
                            }

                newModel =
                    case maybeChangeScene of
                        Just scene ->
                            { model
                            | playerPosition = Tuple.first scene
                            , playerState = initPlayerState
                            , objects = Tuple.second scene
                            , keys = Set.empty
                            }

                        Nothing ->
                            List.foldl
                                consumeObjectUpdate
                                { model
                                | playerPosition = newPlayerPosition
                                , playerState = newPlayerState
                                }
                                objectUpdates
            in
            ( newModel, Cmd.none )

updateKeys : Model -> (Set Int -> Set Int) -> ( Model, Cmd Msg )
updateKeys model fn =
    let
        newKeys = fn model.keys
        newModel = { model | keys = newKeys }
    in
    ( newModel, Cmd.none )

type alias UpdatePlayerResult =
    { newPlayerPosition : Point
    , newPlayerState : PlayerState
    , objectUpdates1 : List ObjectUpdate
    , maybeChangeScene : Maybe Scene
    }

updatePlayer : Model -> UpdatePlayerResult
updatePlayer model =
    let
        -- move to desired position
        applyControl control value =
            if Set.member control model.keys then value else 0
        dx =
            (applyControl controlsLeft -1) +
            (applyControl controlsRight 1)
        dy =
            (applyControl controlsUp -1) +
            (applyControl controlsDown 1)

        ( x, y ) = model.playerPosition
        inBounds i = i |> max 0 |> min (boardSize - 1)
        desiredPosition = ( inBounds (x + dx), inBounds (y + dy) )

        -- attack entities
        attacking = Set.member controlsAttack model.keys
        playerState = model.playerState
        playerState1 = { playerState | attacking = attacking }
        surrounding =
            [ ( x - 1, y - 1 )
            , ( x, y - 1 )
            , ( x + 1, y - 1 )
            , ( x - 1, y )
            , ( x + 1, y )
            , ( x - 1, y + 1 )
            , ( x, y + 1 )
            , ( x + 1, y + 1 )
            ]
        objectUpdates1 =
            if attacking then
                surrounding
                |> List.filterMap maybeMatch
                |> List.map RemoveObject
            else
                []
        maybeMatch point =
            case Dict.get point model.objects of
                Just (Entity _) ->
                    Just point

                _ ->
                    Nothing

        -- interact with objects by movement
        pass =
            { collision = False
            , newPlayerState = playerState1
            , objectUpdates2 = []
            , maybeChangeScene = Nothing
            }
        block = { pass | collision = True }
        maybeObject = Dict.get desiredPosition model.objects
        { collision, newPlayerState, objectUpdates2, maybeChangeScene } =
            case maybeObject of
                Just (Item item) ->
                    let
                        playerState2 =
                            { playerState1
                            | items = item :: playerState1.items
                            }
                        objectUpdate =
                            RemoveObject desiredPosition
                    in
                    { pass
                    | newPlayerState = playerState2
                    , objectUpdates2 = [objectUpdate]
                    }

                Just (Block (Door nextScene condition)) ->
                    let
                        metCondition =
                            case condition of
                                NoCondition ->
                                    True

                                HasItem item ->
                                    List.member item playerState1.items
                    in
                    if metCondition then

                        { pass | maybeChangeScene = Just nextScene }
                    else
                        block

                Just (Text _) ->
                    pass

                Just _ ->
                    block

                _ ->
                    pass

        newPosition =
            if collision then model.playerPosition else desiredPosition

        objectUpdates = objectUpdates1 ++ objectUpdates2
    in
    { newPlayerPosition = newPosition
    , newPlayerState = newPlayerState
    , objectUpdates1 = objectUpdates
    , maybeChangeScene = maybeChangeScene
    }

updateEntity : Model -> Posix -> Point -> EntityType -> List ObjectUpdate
updateEntity model now position entity =
    case entity of
        Butterfly direction lastMoved ->
            updateMoveSideways now position
                Butterfly direction lastMoved

        Bug direction lastMoved ->
            updateMoveSideways now position
                Bug direction lastMoved

        _ ->
            []

updateMoveSideways : Posix -> Point -> (Int -> Int -> EntityType) -> Int -> Int -> List ObjectUpdate
updateMoveSideways now position fn direction lastMoved =
    let
        nowMs = Time.posixToMillis now
        delta = nowMs - lastMoved
        over = delta > entityAnimation
    in
    if over && (lastMoved < entityAnimation) then
        let
            newObject = fn direction (nowMs - lastMoved)
        in

        [ RemoveObject position
        , AddObject position (Entity newObject)
        ]
    else if over then
        let
            ( x, y ) = position
            newPosition = ( x + direction, y )
            newObject = fn (-direction) nowMs
        in
        [ RemoveObject position
        , AddObject newPosition (Entity newObject)
        ]
    else
        []
