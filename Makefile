.PHONY: all clean deps build build-dev

all:
	@echo "usage: clean deps build"

clean:
	rm -rf public node_modules src/elm/elm-stuff

deps:
	npm install --no-save \
		webpack webpack-cli \
		html-webpack-plugin elm-webpack-loader \
		style-loader css-loader sass sass-loader

build:
	node_modules/.bin/webpack

build-dev:
	node_modules/.bin/webpack --mode=development
